import { IsNotEmpty } from 'class-validator';
class CreatedOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  [x: string]: any;
  @IsNotEmpty()
  customerId: number;

  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
